import requests, json
import urllib2
import datetime
import nltk
import nltk.data
from bs4 import BeautifulSoup
from bs4 import SoupStrainer
from time import sleep

def apiRequest(query):
	url = "http://api.peoplegraph.io/v1/search?q="+query+"&api_key=DQawlgWRjwtdieyPltRUhtzDvTSmXcSr"
	print url
	#r = requests.get(url)
	#print r.json
	#print r.headers
	#print r.text

def getTime():
	i = datetime.datetime.now()
	year = str(i.year)
	month = str(i.month)
	day = str(i.day)

	if len(month) == 1:
		month = "0" + month
	if len(day) == 1:
		day = "0" + day
	return (year, month, day)


def buildUrl(date):
	return "http://techcrunch.com"+"/"+date[0]+"/"+date[1]+"/"+date[2]

def getHTMLSoup(url):
	try:
		page = urllib2.urlopen(url).read()
	except urllib2.URLError as e:
		if hasattr(e, "reason"):
			print "We failed to reach a server."
			print "Reason: ", e.reason
        	return

	soup = BeautifulSoup(page)
	soup.prettify()
	if (soup == ""):
		return
	return soup

def grabNewsLinks():
	articles = []

	time = getTime() #get current date
	#rootUrl  = buildUrl(time) #build today's url of articles
	rootUrl = "http://techcrunch.com/2014/07/11"
	soup = getHTMLSoup(rootUrl)#build HTML Soup
	
	for item in soup.find_all("a"):
		if (rootUrl in item.get("href") and not ("#comments" in item.get("href"))):
			articles.append(item.get("href"))

	return set(articles)

def getNamesFromText(text):
	# Performs "named entity recognition" on the text 
	#nltk.download()
	
	tokens = nltk.tokenize.word_tokenize(text)
	pos = nltk.pos_tag(tokens)
	sentt = nltk.ne_chunk(pos, binary = False)
	person_list = []
	person = []
	name = ""
	for subtree in sentt.subtrees(filter=lambda t: t.node == 'PERSON'):
		for leaf in subtree.leaves():
			person.append(leaf[0])
		if len(person) > 1: #avoid grabbing lone surnames
			for part in person:
				name += part + ' '
			if name[:-1] not in person_list:
				person_list.append(name[:-1])
			name = ''
		person = []

	return (person_list)


def getPeople():
	people = []

	links = grabNewsLinks()

	for link in links:
		soup = getHTMLSoup(link)
		if (soup.find_all(rel="author")):#get author
			# I assumed an article has only one author
			people.append(soup.find_all(rel="author")[0].text)
		if (soup.select(".article-entry")):
			text = soup.find('p').getText() #get text from <p> tags
			names = getNamesFromText(text) #get the names
			for name in names:
				people.append(name)
	
	return set(people)

def main():
	people = getPeople()

	for person in people:
		apiRequest(person)
		sleep(2)#need to sleep for a bit

if __name__ == '__main__':
    main()